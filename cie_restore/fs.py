# SPDX-FileCopyrightText: 2023 Coop IT Easy SC
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import argparse
import pathlib
import shutil
import subprocess
import sys

from .util import name_of_file

DEFAULT_TARGET = pathlib.Path("~/.local/share/Odoo/filestore").expanduser()


def create_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(description="Restore a filesystem.")
    parser.add_argument(
        "filesystem",
        action="store",
        help="path to filesystem directory to restore",
    )
    parser.add_argument(
        "--new-db-name",
        "-n",
        action="store",
        help="name of database to restore to",
    )
    parser.add_argument(
        "--delete-existing",
        "-D",
        action="store_true",
        help="delete existing filestore before restoration",
    )
    parser.add_argument(
        "--odoo-fs",
        action="store",
        help="odoo filestore directory to restore to",
    )
    return parser


def delete_fs(db_name, fs_target=None):
    if fs_target is None:
        fs_target = DEFAULT_TARGET
    target = pathlib.Path(fs_target) / db_name
    try:
        shutil.rmtree(target)
        print(f"deleted '{target}'")
    except FileNotFoundError:
        pass


def restore_fs(from_path, db_name, fs_target=None):
    if fs_target is None:
        fs_target = DEFAULT_TARGET

    target = pathlib.Path(fs_target) / db_name
    print("starting filesystem restoration. this may take a while")
    shutil.copytree(from_path, target, dirs_exist_ok=True)
    print(f"copied '{from_path}' to '{target}'")


def main():
    parser = create_parser()
    args = parser.parse_args()

    new_db_name = args.new_db_name or name_of_file(args.filesystem)
    if args.delete_existing:
        delete_fs(new_db_name, fs_target=args.odoo_fs)

    restore_fs(args.filesystem, new_db_name, fs_target=args.odoo_fs)

    return 0


if __name__ == "__main__":
    sys.exit(main())
