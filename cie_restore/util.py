# SPDX-FileCopyrightText: 2023 Coop IT Easy SC
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import pathlib


def name_of_file(path):
    return pathlib.Path(path).name
