# SPDX-FileCopyrightText: 2023 Coop IT Easy SC
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import argparse
import contextlib
import pathlib
import subprocess
import sys
import tempfile

from .db import drop_db, restore_db
from .download import download, find_repo, from_iso
from .fs import delete_fs, restore_fs


def create_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        description="Download and restore an archive of a database."
    )
    parser.add_argument(
        "--server",
        "-s",
        action="store",
        help="server from which to download backup",
        required=True,
    )
    parser.add_argument(
        "--date",
        "-d",
        action="store",
        help="latest datetime (ISO 8601) you want a backup from; backups after"
        " this date are not considered",
    )
    parser.add_argument(
        "--target",
        "-t",
        action="store",
        help="path to directory to extract archive to, optional",
    )
    parser.add_argument(
        "--new-db-name",
        "-n",
        action="store",
        help="new name of database",
    )
    parser.add_argument(
        "--jobs",
        "-j",
        action="store",
        type=int,
        help="amount of parallel jobs for database restoration",
    )
    parser.add_argument(
        "--delete-existing",
        "-D",
        action="store_true",
        help="drop existing database and delete existing filestore before restoration",
    )
    parser.add_argument(
        "--odoo-fs",
        action="store",
        help="odoo filestore directory to restore to",
    )
    parser.add_argument(
        "--prod",
        "-p",
        action="store_true",
        help="a posthook that disables all crons and e-mails is run unless this"
        " flag is used",
    )
    parser.add_argument(
        "database",
        action="store",
        help="name of database to download",
    )
    return parser


@contextlib.contextmanager
def directory_context(target=None):
    if target is None:
        temp_dir = tempfile.TemporaryDirectory()
        try:
            yield temp_dir.name
        finally:
            # clean up temporary directory when exiting context
            temp_dir.cleanup()
    else:
        # don't clean up anything when a target is specified; permanency is
        # expected.
        yield target


def find_db(archive_dir):
    archive_dir = pathlib.Path(archive_dir)
    return next(archive_dir.glob(".borgmatic/*/postgresql_databases/*/*"))


def find_fs(archive_dir):
    archive_dir = pathlib.Path(archive_dir)
    return next(archive_dir.glob("filestore/*"))


def main():
    parser = create_parser()
    args = parser.parse_args()

    new_db_name = args.new_db_name or args.database

    # Download
    date = None
    if args.date:
        date = from_iso(args.date)

    repo = find_repo(args.server, args.database)

    with directory_context(args.target) as target:
        download(args.server, repo, target, target_date=date)

        # Restore db
        if args.delete_existing:
            drop_db(new_db_name)

        restore_db(find_db(target), new_db_name, jobs=args.jobs, prod=args.prod)

        # Restore fs
        if args.delete_existing:
            delete_fs(new_db_name, fs_target=args.odoo_fs)

        restore_fs(find_fs(target), new_db_name, fs_target=args.odoo_fs)

        return 0


if __name__ == "__main__":
    sys.exit(main())
