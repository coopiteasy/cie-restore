# SPDX-FileCopyrightText: 2023 Coop IT Easy SC
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import argparse
import datetime
import json
import os
import pathlib
import shutil
import subprocess
import sys
import tempfile
from getpass import getpass

BORG_USER = "borgread"


def create_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(description="Download an archive of a database.")
    parser.add_argument(
        "--server",
        "-s",
        action="store",
        help="server from which to download backup",
        required=True,
    )
    parser.add_argument(
        "--date",
        "-d",
        action="store",
        help="latest datetime (ISO 8601) you want a backup from; backups after"
        " this date are not considered",
    )
    parser.add_argument(
        "--target",
        "-t",
        action="store",
        help="path to directory to extract archive to, optional",
    )
    parser.add_argument(
        "database",
        action="store",
        help="name of database to download",
    )
    return parser


def from_iso(iso: str) -> datetime.datetime:
    """Convert an ISO 8601 string to a datetime. If the string already contains
    time information, preserve it. If it does not, set the time information to
    their latest values.
    """
    try:
        result_date = datetime.date.fromisoformat(iso)
        return datetime.datetime.combine(result_date, datetime.datetime.max.time())
    except ValueError:
        pass
    return datetime.datetime.fromisoformat(iso)


def clear_directory(path):
    """Delete everything inside of a directory without deleting the directory
    itself.
    """
    path = pathlib.Path(path)
    if path.is_dir():
        for item in pathlib.Path(path).iterdir():
            if item.is_file():
                item.unlink()
            elif item.is_dir():
                shutil.rmtree(item)


def mkdir_p(path):
    """Make directory and its parents."""
    pathlib.Path(path).mkdir(parents=True, exist_ok=True)


def authenticate():
    if not os.environ.get("BORG_PASSPHRASE"):
        os.environ["BORG_PASSPHRASE"] = getpass(
            prompt="Enter passphrase of borg repository: "
        )


def get_list_of_archives(server, repo_name):
    list_result = subprocess.run(
        [
            "borg",
            "--bypass-lock",
            "list",
            "--json",
            f"{BORG_USER}@{server}:{repo_name}",
        ],
        capture_output=True,
        check=True,
    )
    return json.loads(list_result.stdout)


def get_latest_archive(archives_dict, target_date=None):
    if target_date is None:
        # Roundabout way of getting the latest time for today.
        target_date = from_iso(datetime.date.today().isoformat())
    delta = datetime.timedelta.max
    zero_delta = datetime.timedelta()
    latest_archive = None
    for archive in archives_dict:
        archive_date = datetime.datetime.fromisoformat(archive["time"])
        if (
            new_delta := (target_date - archive_date)
        ) < delta and new_delta >= zero_delta:
            delta = new_delta
            latest_archive = archive
    if latest_archive is not None:
        return latest_archive["archive"]
    raise ValueError(f"could not find any archive before {target_date}")


def download_archive(server, repo_name, archive, destination):
    clear_directory(destination)
    mkdir_p(destination)
    subprocess.run(
        [
            "borg",
            # lock is bypassed because BORG_USER may not be allowed to write to
            # the lock file.
            "--bypass-lock",
            "extract",
            "--progress",
            f"{BORG_USER}@{server}:{repo_name}::{archive}",
        ],
        check=True,
        cwd=destination,
    )


def find_repo(server, dbname):
    # This _cannot_ be run using the regular borg user as presently configured
    # (2023-05-24). The regular borg user is strictly restricted to the `borg
    # serve` command.
    #
    # Instead, we run this with a special read-only user that is in the same
    # group as the borg user. It is able to get a list of directories using the
    # below command, which we can then use to find the repo we want/need.
    find_result = subprocess.run(
        [
            "ssh",
            f"{BORG_USER}@{server}",
            "--",
            "find",
            "/borg",
            "-maxdepth",
            "1",
            "-mindepth",
            "1",
            "-print0",
        ],
        capture_output=True,
        check=True,
    )
    repos = find_result.stdout.decode("utf-8").split("\0")
    for repo in repos:
        # TODO: Handle multiple repos with the same name.
        if repo.endswith(f"-{dbname}"):
            print(f"found repo '{server}:{repo}'")
            return repo
    raise ValueError(f"could not find repository for {dbname}")


def download(server, repo_name, destination, target_date=None):
    """Fairly meta everything-method."""
    authenticate()

    list_result_dict = get_list_of_archives(server, repo_name)

    archive = get_latest_archive(list_result_dict["archives"], target_date=target_date)
    print(f"found archive '{archive}'")

    download_archive(server, repo_name, archive, destination)
    print(f"extracted archive to '{destination}'")


def main():
    parser = create_parser()
    args = parser.parse_args()

    date = None
    if args.date:
        date = from_iso(args.date)

    repo = find_repo(args.server, args.database)

    if not (target := args.target):
        target = tempfile.TemporaryDirectory().name

    download(args.server, repo, target, target_date=date)

    return 0


if __name__ == "__main__":
    sys.exit(main())
